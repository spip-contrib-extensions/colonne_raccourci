<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'colonne_description' => 'Créer des colonnes responsives avec le raccourci &lt;colonne&gt; voir /?page=demo/colonne',
	'colonne_nom' => 'Raccourci colonne',
	'colonne_slogan' => 'Des colonnes responsives dans le texte',
);
